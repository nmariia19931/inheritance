package ru.tsystems.autotest;

public abstract class MediaPost extends AbstractPost {
    private String fileName;
    private String format;

    public MediaPost(String author, String message, PostType postType, String fileName, String format) {
        super(author, message, postType);
        this.fileName = fileName;
        this.format = format;
    }

    @Override
    public void printPostInfo() {
        super.printPostInfo();
        System.out.println("File name is " + this.getFileName());
        System.out.println("Format of file is " + this.getFormat());
    }

    public String getFileName() {
        return fileName;
    }

    public String getFormat() {
        return format;
    }

    public abstract int getMaxSize();
}
