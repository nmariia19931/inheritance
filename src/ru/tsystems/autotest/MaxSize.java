package ru.tsystems.autotest;

public enum MaxSize {
    TEXT(15),
    IMAGE(40),
    VIDEO(500),
    AUDIO(20);

    private int size;

    MaxSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
