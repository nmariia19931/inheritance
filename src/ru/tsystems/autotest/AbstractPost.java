package ru.tsystems.autotest;

import java.util.Date;

/**
 * Abstract class with the post's structure
 */
public abstract class AbstractPost {
    private String author;
    private Date date;
    private String message;
    private PostType postType;

    public AbstractPost(String author, String message, PostType postType) {
        this.author = author;
        this.message = message;
        this.date = new Date();
        this.postType = postType;
    }

    public String getAuthor() {
        return author;
    }

    public Date getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public PostType getPostType() {
        return postType;
    }

    public void printPostInfo() {
        System.out.println("Post was created by " + author + " at " + date);
        System.out.println("Post type is " + postType);
        System.out.println("Post contains the following message: " + message);
    }

    public abstract int getMaxSize();
}