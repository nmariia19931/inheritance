package ru.tsystems.autotest;

public enum PostType {
    NOTE,
    REPOST,
    COMMENT;
}
