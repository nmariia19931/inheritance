package ru.tsystems.autotest;

public class Main {

    public static void main(String[] args) {
        TextPost textPost1 = new TextPost("Mariia", "My #first text post", PostType.NOTE);
        textPost1.printPost();
        printGeneralnfo(textPost1);
        TextPost textPost2 = new TextPost("Mariia", "Comment to #first text post", PostType.COMMENT);
        textPost2.printPost();
        printGeneralnfo(textPost2);
        ImagePost imagePost = new ImagePost("Mariia", "My image post", PostType.NOTE, "1",
                "jpg", 1024, 1024);
        imagePost.printPost();
        printGeneralnfo(imagePost);
        VideoPost videoPost = new VideoPost("Mariia", "My video post", PostType.REPOST, "2",
                "avi", "3:10", 1920, 1080);
        videoPost.playPost();
        printGeneralnfo(videoPost);
        AudioPost audioPost = new AudioPost("Mariia", "My audio post", PostType.NOTE, "3",
                "mp3", "3:20", 320);
        audioPost.playPost();
        printGeneralnfo(audioPost);
    }

    private static void printGeneralnfo(AbstractPost abstractPost) {
        System.out.println("Max size of " + abstractPost.getClass().getSimpleName() + " is " + abstractPost.getMaxSize() + "mb");
        abstractPost.printPostInfo();
        System.out.println("/////////////////////////////////////////////////////////");
    }
}
