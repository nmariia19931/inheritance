package ru.tsystems.autotest;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextPost extends AbstractPost implements IPrintPost {
    private static final Pattern MY_PATTERN = Pattern.compile("#(\\S+)");
    private List<String> hashtags;

    public TextPost(String author, String message, PostType postType) {
        super(author, message, postType);
        this.hashtags = findHashtags(message);
    }

    public int getMaxSize() {
        return MaxSize.TEXT.getSize();
    }

    public void printPost() {
        System.out.println("Text of the post: " + getMessage());
    }

    @Override
    public void printPostInfo() {
        super.printPostInfo();
        System.out.println("Hashtags used in the post: " + this.getHashtags());
    }

    private List<String> findHashtags(String message) {
        Matcher mat = MY_PATTERN.matcher(message);
        List<String> hashtags = new ArrayList<String>();
        while (mat.find()) {
            hashtags.add(mat.group(1));
        }
        return hashtags;
    }

    public List<String> getHashtags() {
        return hashtags;
    }
}
