package ru.tsystems.autotest;

public class VideoPost extends MediaPost implements IPlayPost {
    private String duration;
    private int frameWidth;
    private int frameHeight;

    public VideoPost(String author, String message, PostType postType, String fileName, String format, String duration,
                     int frameWidth, int frameHeight) {
        super(author, message, postType, fileName, format);
        this.duration = duration;
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
    }

    public void playPost() {
        System.out.println("The video will be played in 10 sec");
    }

    @Override
    public void printPostInfo() {
        super.printPostInfo();
        System.out.println("Video duration: " + this.getDuration());
        System.out.println("Video frame width: " + this.getFrameWidth());
        System.out.println("Video frame height: " + this.getFrameHeight());
    }

    public String getDuration() {
        return duration;
    }

    public int getFrameWidth() {
        return frameWidth;
    }

    public int getFrameHeight() {
        return frameHeight;
    }

    @Override
    public int getMaxSize() {
        return MaxSize.VIDEO.getSize();
    }
}

