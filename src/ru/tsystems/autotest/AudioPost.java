package ru.tsystems.autotest;

public class AudioPost extends MediaPost implements IPlayPost {
    private String duration;
    private int bitRate;

    public AudioPost(String author, String message, PostType postType, String fileName, String format, String duration,
                     int bitRate) {
        super(author, message, postType, fileName, format);
        this.duration = duration;
        this.bitRate = bitRate;
    }

    public void playPost() {
        System.out.println("The audio will be played in 10 sec");
    }

    @Override
    public void printPostInfo() {
        super.printPostInfo();
        System.out.println("Audio duration: " + this.getDuration());
        System.out.println("Audio bit rate: " + this.getBitRate());
    }

    public String getDuration() {
        return duration;
    }

    public int getBitRate() {
        return bitRate;
    }

    @Override
    public int getMaxSize() {
        return MaxSize.AUDIO.getSize();
    }
}
