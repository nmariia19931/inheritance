package ru.tsystems.autotest;

public class ImagePost extends MediaPost implements IPrintPost {
    private int width;
    private int height;

    public ImagePost(String author, String message, PostType postType, String fileName, String format, int width, int height) {
        super(author, message, postType, fileName, format);
        this.width = width;
        this.height = height;
    }

    public void printPost() {
        System.out.println("The image has width " + width + " and height " + height);
    }

    @Override
    public void printPostInfo() {
        super.printPostInfo();
        System.out.println("Width of image is " + this.getWidth());
        System.out.println("Height of image is " + this.getHeight());
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public int getMaxSize() {
        return MaxSize.IMAGE.getSize();
    }
}
